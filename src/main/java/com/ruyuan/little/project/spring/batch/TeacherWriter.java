package com.ruyuan.little.project.spring.batch;

import com.ruyuan.little.project.spring.dto.Teacher;
import com.ruyuan.little.project.spring.mapper.TeacherMapper;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:spring实战
 **/
public class TeacherWriter implements ItemWriter<Teacher> {

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public void write(List<? extends Teacher> list) throws Exception {
        for (Teacher teacher : list){
            int update = teacherMapper.updateTeachingDays(teacher);
        }
    }
}
